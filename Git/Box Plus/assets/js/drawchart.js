Chart.pluginService.register({
  afterDraw: function(chart) {
    if (chart.config.options.elements.center) {
      var helpers = Chart.helpers;
      var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
      var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;

      var ctx = chart.chart.ctx;
      ctx.save();
      var fontSize = helpers.getValueOrDefault(chart.config.options.elements.center.fontSize, Chart.defaults.global.defaultFontSize);
      var fontStyle = helpers.getValueOrDefault(chart.config.options.elements.center.fontStyle, Chart.defaults.global.defaultFontStyle);
      var fontFamily = helpers.getValueOrDefault(chart.config.options.elements.center.fontFamily, Chart.defaults.global.defaultFontFamily);
      var font = helpers.fontString(fontSize, fontStyle, fontFamily);
      ctx.font = font;
      ctx.fillStyle = helpers.getValueOrDefault(chart.config.options.elements.center.fontColor, Chart.defaults.global.defaultFontColor);
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.fillText(chart.config.options.elements.center.text, centerX, centerY);
      ctx.restore();
    }
  },
})


// change the value of the 5 item;


var heineikenpercent = [50,30,10,10,100];
var tigerpercent=      [20,30,10,20,0];
var budweipercent=     [05,20,30,30,0];
var saigonredpercent=  [10,10,30,20,0];
var saigonpercent=     [05,15,15,15,0];
var otherpercent=      [10,05,05,05,0];

// color for heineiken - tiger - budwei - saigonred - saigon - other

var totaltool = 4;

var color=["#5e9bd2","#ea7d3c","#8b572a","#fcbe2d","#4674c0","#72ac4d"];



var countedpercentArr= [0,50,20,05,10,05];

// var heineiken = String(heineikenpercent[]) + "%";
var getOption = function(_percentSet, _pos,_color) {
  return {
    type: 'pie',
    data: {
      labels: [
        "Red",
        "Green",
        "Yellow"
      ],
      datasets: [{
        data: [countedpercentArr[_pos], _percentSet[_pos], 100-(_percentSet[_pos])],
        backgroundColor: [
          "red",
          color[_color],
          "white"
        ],
        hoverBackgroundColor: [
          "white",
          "#5e9bd2",
          "white"
        ]
      }]
    },
    options: {
      elements: {
        center: {
          text: _percentSet[_pos].toString() + '%',
          fontColor: '#000',
          fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
          fontSize: 15,
          fontStyle: 'normal'
        }
      },
      // elements: {
      //   arc: {
      //       borderWidth: 0
      //   }
      // },
      segmentShowStroke: false,
      tooltips :{
          enabled: false
      },
      responsive: true,
      legend: {
        display: false
      }
    }
  };
}


var heineikenconfigArr = [0,0,0];

for (i = 0; i <= totaltool; i++) {
   heineikenconfigArr.push(getOption(heineikenpercent, i  ,0));
 }





console.log (countedpercentArr);


var tigerconfigArr = [];

for (i = 0; i <= totaltool; i++) {
   tigerconfigArr.push(getOption(tigerpercent, i  ,1));

 } 



var budweiconfigArr = [];

for (i = 0; i <= totaltool; i++) {
   budweiconfigArr.push(getOption(budweipercent, i  ,2));
 } 

var saigonredconfigArr = [];

for (i = 0; i <= totaltool; i++) {
   saigonredconfigArr.push(getOption(saigonredpercent, i  ,3));
 } 

var saigonconfigArr = [];

for (i = 0; i <= totaltool; i++) {
   saigonconfigArr.push(getOption(saigonpercent, i  ,4));
 } 

var otherconfigArr = [];

for (i = 0; i <= totaltool; i++) {
   otherconfigArr.push(getOption(otherpercent, i  ,5));
 } 



var ctx = document.getElementById("heineikenchart").getContext("2d");
var heineikenchart = new Chart(ctx, heineikenconfigArr[0]);

var ctx = document.getElementById("tigerchart").getContext("2d");
var tigerchart = new Chart(ctx, tigerconfigArr[0]);

var ctx = document.getElementById("budweichart").getContext("2d");
var budweichart = new Chart(ctx, budweiconfigArr[0]);

var ctx = document.getElementById("saigonredchart").getContext("2d");
var saigonredchart = new Chart(ctx, saigonredconfigArr[0]);

var ctx = document.getElementById("saigonchart").getContext("2d");
var saigonchart = new Chart(ctx, saigonconfigArr[0]);

var ctx = document.getElementById("otherchart").getContext("2d");
var otherchart = new Chart(ctx, otherconfigArr[0]);

var ctx = document.getElementById("heineikenchart1").getContext("2d");
var heineikenchart1 = new Chart(ctx, heineikenconfigArr[1]);

var ctx = document.getElementById("tigerchart1").getContext("2d");
var tigerchart1 = new Chart(ctx, tigerconfigArr[1]);

var ctx = document.getElementById("budweichart1").getContext("2d");
var budweichart1 = new Chart(ctx, budweiconfigArr[1]);

var ctx = document.getElementById("saigonredchart1").getContext("2d");
var saigonredchart1 = new Chart(ctx, saigonredconfigArr[1]);

var ctx = document.getElementById("saigonchart1").getContext("2d");
var saigonchart1 = new Chart(ctx, saigonconfigArr[1]);

var ctx = document.getElementById("otherchart1").getContext("2d");
var otherchart1 = new Chart(ctx, otherconfigArr[1]);

var ctx = document.getElementById("heineikenchart2").getContext("2d");
var heineikenchart2 = new Chart(ctx, heineikenconfigArr[2]);

var ctx = document.getElementById("tigerchart2").getContext("2d");
var tigerchart2 = new Chart(ctx, tigerconfigArr[2]);

var ctx = document.getElementById("budweichart2").getContext("2d");
var budweichart2 = new Chart(ctx, budweiconfigArr[2]);

var ctx = document.getElementById("saigonredchart2").getContext("2d");
var saigonredchart2 = new Chart(ctx, saigonredconfigArr[2]);

var ctx = document.getElementById("saigonchart2").getContext("2d");
var saigonchart2 = new Chart(ctx, saigonconfigArr[2]);

var ctx = document.getElementById("otherchart2").getContext("2d");
var otherchart2 = new Chart(ctx, otherconfigArr[2]);



var ctx = document.getElementById("heineikenchart3").getContext("2d");
var heineikenchart3 = new Chart(ctx, heineikenconfigArr[3]);

var ctx = document.getElementById("tigerchart3").getContext("2d");
var tigerchart3 = new Chart(ctx, tigerconfigArr[3]);

var ctx = document.getElementById("budweichart3").getContext("2d");
var budweichart3 = new Chart(ctx, budweiconfigArr[3]);

var ctx = document.getElementById("saigonredchart3").getContext("2d");
var saigonredchart3 = new Chart(ctx, saigonredconfigArr[3]);

var ctx = document.getElementById("saigonchart3").getContext("2d");
var saigonchart3 = new Chart(ctx, saigonconfigArr[3]);

var ctx = document.getElementById("otherchart3").getContext("2d");
var otherchart = new Chart(ctx, otherconfigArr[3]);


var ctx = document.getElementById("heineikenchart4").getContext("2d");
var heineikenchart4 = new Chart(ctx, heineikenconfigArr[4]);

var ctx = document.getElementById("tigerchart4").getContext("2d");
var tigerchart4 = new Chart(ctx, tigerconfigArr[4]);

var ctx = document.getElementById("budweichart4").getContext("2d");
var budweichart4 = new Chart(ctx, budweiconfigArr[4]);

var ctx = document.getElementById("saigonredchart4").getContext("2d");
var saigonredchart4 = new Chart(ctx, saigonredconfigArr[4]);

var ctx = document.getElementById("saigonchart4").getContext("2d");
var saigonchart4 = new Chart(ctx, saigonconfigArr[4]);

var ctx = document.getElementById("otherchart4").getContext("2d");
var otherchart4 = new Chart(ctx, otherconfigArr[4]);
