$(document).ready(function(){
	$('#btnSale').on('click', function(){
		if($(this).hasClass('dark-blue')){
			$(this).removeClass('dark-blue');
			$('#attribSale').val("0");
		}
		else{
			$(this).addClass('dark-blue');
			$('#attribSale').val("1");
		}

	});

	$('#btnFeature').on('click', function(){
		if($(this).hasClass('dark-blue')){
			$(this).removeClass('dark-blue');
			$('#attribFeature').val("0");
		}
		else{
			$(this).addClass('dark-blue');
			$('#attribFeature').val("1");
		}

	});

	$('#btnNew').on('click', function(){
		if($(this).hasClass('dark-blue')){
			$(this).removeClass('dark-blue');
			$('#attribNew').val("0");
		}
		else{
			$(this).addClass('dark-blue');
			$('#attribNew').val("1");
		}

	});

	$('#btnBestseller').on('click', function(){
		if($(this).hasClass('dark-blue')){
			$(this).removeClass('dark-blue');
			$('#attribBestseller').val("0");
		}
		else{
			$(this).addClass('dark-blue');
			$('#attribBestseller').val("1");
		}

	});


});