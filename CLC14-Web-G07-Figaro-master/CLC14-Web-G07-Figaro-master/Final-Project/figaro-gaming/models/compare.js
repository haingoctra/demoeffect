module.exports = function Compare(oldCompare){
	this.items = oldCompare.items || {};
	this.count = oldCompare.count || 0;
	this.totalPrice = oldCompare.totalPrice || 0;
	this.add = function(item, id, quan){
		var storedItem = this.items[id];

		if(!storedItem){
			storedItem = this.items[id] = {item: item, price: 0};
		}

		var addPrice;
		// if(storedItem.item.onsale)
		// 	addPrice = Number((storedItem.item.pricesale).replace(/[^0-9\.]+/g,"")) * quan;
		// else
		// 	addPrice = Number((storedItem.item.price).replace(/[^0-9\.]+/g,"")) * quan;

		if(storedItem.item.onsale)
			addPrice = Number((storedItem.item.pricesale));
		else
			addPrice = Number((storedItem.item.price));

		// storedItem.price += addPrice;
		// storedItem.price.toFixed(2);

		storedItem.price += addPrice;
		//storedItem.price.toFixed(2);

		this.count += quan;
		this.totalPrice += addPrice;
	}
	this.toArray = function(){
		var a = [];
		for(var id in this.items){
			a.push(this.items[id]);
		}
		return a;
	}
}