var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var orderSchema = new Schema({
		// id: {type: String, required: true},

		cardnumber: [String],
		expday: [String],
		expmonth: [String],
		securecode: [String],

		
		receiver: {
	  		name: [String],
			city: [String],
			email: [String],
			portal: [String],
			phone: [String],
	  	},

		buyer: {
	  		name:[String],
	  		email:[String],
	  		phone:[String]
	  	},

});

module.exports = mongoose.model('Billing', productSchema);



