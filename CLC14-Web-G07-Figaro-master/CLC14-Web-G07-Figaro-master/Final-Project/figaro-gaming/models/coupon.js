var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var couponSchema = new Schema({
  	name: {type: String, required: true},
  	code: {type: String, required: true},
  	startDate: {type: String},
    expiredDate: {type: String}
});


module.exports = mongoose.model('Coupon', couponSchema);

