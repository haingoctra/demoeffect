var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var newsSchema = new Schema({
  	title: {type: String, required: true},
  	img: String,
  	
  	detailimg :[String],
  	summary: String,
  	description: {
  		header :[String],
  		text : [String],
  		detailimg : [String],
  		youtubelink :[String]
  	},
  	time : { type : Date, default: Date.now },
});


module.exports = mongoose.model('News', newsSchema);

