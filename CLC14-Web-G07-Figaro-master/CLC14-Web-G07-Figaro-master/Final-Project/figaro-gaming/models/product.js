var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var productSchema = new Schema({
		// id: {type: String, required: true},
	  	name: {type: String, required: true},
	  	category: String,
	  	img: [String],
	  	fancyboximg: [String],
	  	manufacturer:String,
	  	availability: String,
	  	price: String,
	  	pricesale: String,
	  	genre:String,
	  	featured: {type: Boolean, default: false},
	  	recentlyrelease: {type: Boolean, default: false},
	  	bestseller: {type: Boolean, default: false},
	  	onsale: {type: Boolean, default: false},
	  	descriptions: {
	  		header:[String],
	  		text:[String],
	  		list:[String]

	  	},
	  	requirement: [String],
	  	tag: [String],
	  	website: String,
	  	review:[{

	  	text: String,
        time : { type : Date, default: Date.now },
        postedBy: String,
        rate: String  		

	  	}]
	
});

module.exports = mongoose.model('Product', productSchema);



