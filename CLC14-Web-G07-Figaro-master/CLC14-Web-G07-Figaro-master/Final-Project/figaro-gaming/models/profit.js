var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var profitSchema = new Schema({
	name : String,
	year : String,
	jan :[Number],
	feb:[Number],
	mar :[Number],
	apr : [Number],
	may :[Number],
	june :[Number],
	july :[Number],
	aug: [Number],
	sept :[Number],
	oct :[Number],
	nov:[Number],
	dec:[Number]		

	}
);

module.exports = mongoose.model('Profit', profitSchema);