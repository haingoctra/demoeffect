module.exports = function Cart(oldCart){
	this.items = oldCart.items || {};
	this.count = oldCart.count || 0;
	this.totalPrice = oldCart.totalPrice || 0;
	this.add = function(item, id, quan){
		var storedItem = this.items[id];

		if(!storedItem){
			storedItem = this.items[id] = {item: item, quantity: 0, price: 0};
		}

		storedItem.quantity += quan;
		var addPrice;
		if(storedItem.item.onsale)
			addPrice = Number((storedItem.item.pricesale).replace(/[^0-9\.]+/g,"")) * quan;
		else
			addPrice = Number((storedItem.item.price).replace(/[^0-9\.]+/g,"")) * quan;

		// if(storedItem.item.onsale)
		// 	addPrice = Number((storedItem.item.pricesale)) * quan;
		// else
		// 	addPrice = Number((storedItem.item.price)) * quan;

		// storedItem.price += addPrice;
		// storedItem.price.toFixed(2);

		storedItem.price += addPrice;
		storedItem.price.toFixed(2);

		this.count += quan;
		this.totalPrice += addPrice;
	}
	this.remove = function(id){
		var removedItem = this.items[id];

		// if(!removedItem){
		// 	return;
		// }

		//subtract the price + cart item quantity
		this.count -= removedItem.quantity;
		this.totalPrice -= removedItem.price;

		delete this.items[id];
	}
	this.toArray = function(){
		var a = [];
		for(var id in this.items){
			a.push(this.items[id]);
		}
		return a;
	}
}