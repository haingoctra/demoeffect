const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'kingofmoba@gmail.com',
        pass: 'thephantomqn'
    }
});

// // setup email data with unicode symbols
// let mailOptions = {
//     from: '"Kid" <kingofmoba@gmail.com>', // sender address
//     to: 'erocosplayvideo@gmail.com', // list of receivers
//     subject: 'Hello ✔', // Subject line
//     text: 'Hello world ?', // plain text body
//     html: '<b>Hello world ?</b>' // html body
// };

// transporter.sendMail(mailOptions, (error, info) => {
// });


module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true
    },
    function(req, username, password, done) {

        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {
        User.findOne({ 'local.username' :  username }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);

            // validate fields
            req.check('email', 'Email không hợp lệ').isEmail();
            req.check('password', 'Mật khẩu tối thiếu 6 ký tự').isLength({min:6});
            req.check('password', 'Mật khẩu không giống nhau').equals(req.body.passwordconf);

            var errors = req.validationErrors();
            if(errors){
                // req.session.errors = error;
                errors.forEach(function(currentValue){
                    req.flash('signupMessage', currentValue.msg);
                });
                return done(null, false);
            }

            // check to see if theres already a user with that email
            if (user) {
                return done(null, false, req.flash('signupMessage', 'Username trùng'));
            } else {

                // if there is no user with that email
                // create the user
                var newUser = new User();
			    // set the user's local credentials
		        newUser.local.username = username;                                                                                                                 
		        newUser.local.password = newUser.generateHash(password);
		        newUser.local.email = req.body.email;
                
                let mailOptions = {
                    from: '"Kid" <kingofmoba@gmail.com>', // sender address
                    to: newUser.local.email, // list of receivers
                    subject: 'Welcome to Figaro gaming', // Subject line
                    text: 'Thư thông báo đăng kí thành công!', // plain text body
                    html: '<b>Hello world ?</b>' // html body
                };

                
                // save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    else{
                        transporter.sendMail(mailOptions, (error, info) => {
                        });
                    }
                    return done(null, newUser);
                });
            }
        });    
        });
    }));

    passport.use('local-login', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, username, password, done) { 
    	console.log('local-login');
        User.findOne({ 'local.username' :  username }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err){
            	console.log(err);
                return done(err);
            }

            // if no user is found, return the message
            if (!user){
            	console.log('No user found');
                return done(null, false, req.flash('loginMessage', 'Username không tồn tại')); // req.flash is the way to set flashdata using connect-flash
            }

            // if the user is found but the password is wrong
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Sai mật khẩu')); // create the loginMessage and save it to session as flashdata

            // if user is locked
            if(user.local.locked)
                return done(null, false, req.flash('loginMessage', 'Tài khoản đã bị khóa'));
            
            console.log('login succesfully');
            // all is well, return successful user


            return done(null, user);
        });

    }));

};