var express = require('express');
var router = express.Router();
var Product = require('../models/product');
var Coupon = require('../models/coupon');
var User = require('../models/user');
var Profit = require('../models/profit');
var csrf = require('csurf');
var csrfProtection = csrf();
router.use(csrfProtection);

// check admin accout for every request
router.use(function(req, res, next){
	var _cart = [];
  if (req.session.cart) {
    var myCart = new Cart(req.session.cart);
    _cart = myCart.toArray();
  }
  var _compare = [];
  if (req.session.compare) {
    var myCompare = new Compare(req.session.compare);
    _compare = myCompare.toArray();
  }
	if(req.user == undefined || req.user.local.type != 'admin')
		 res.render('404', { title: 'Figaro gaming page not found', _csrfToken: req.csrfToken(), user: req.user, cart : _cart,compare: _compare  });
	next();
});

router.get('/', function(req, res, next){
	Product.find(function(err, products, count){
		res.render('product_list', { products: products, _csrfToken: req.csrfToken() });
	});
});

router.post('/delete_product', function(req, res, next){
	Product.find({ _id: req.body.id}).remove().exec();
	Product.find(function(err, users, count){
		res.redirect('/admin');
	});
});

router.get('/report', function(req, res, next){


			var d = new Date();
//comment
			var mar = [];

			var curmonth = d.getMonth();

			//var curmonth = 0;

			//var curyear = d.getFullYear();

			var curyear = 2017;

			var charid;

			if(req.query.curmonth){
			curmonth = req.query.curmonth;
			}

			if(req.query.curyear){
			curyear = req.query.curyear;
			}

			if (curmonth == 0){
				charid = "dayincomejan";
			}
			else {
				if (curmonth == 1){
				charid = "dayincomefeb";
				}
			 if (curmonth == 2){
				charid = "dayincomemar";
			}
			 if (curmonth == 3){
				charid = "dayincomeapr";
			}
			 if (curmonth == 4){
				charid = "dayincomemay";
			}
			 if (curmonth == 5){
				charid = "dayincomejune";
			}
			 if (curmonth == 6){
				charid = "dayincomejuly";
			}
			 if (curmonth == 7){
				charid = "dayincomeaug";
			}
			 if (curmonth == 8){
				charid = "dayincomesept";
			}
			 if (curmonth == 9){
				charid = "dayincomeoct";
			}
			 if (curmonth == 10){
				charid = "dayincomenov";
			}
			 if (curmonth == 11){
				charid = "dayincomedec";
			}
			}
			
			Profit.findOne({ year: curyear})
			.exec(function(err, profits, count){
				res.render('report', { profits:profits,curmonth:curmonth,curyear:curyear,charid:charid,_csrfToken: req.csrfToken() });
			});
});





router.get('/edit_product/:id', function(req, res, next){
	Product.findById(req.params.id, function(err, product){
		res.render('edit_product', {product: product, _csrfToken: req.csrfToken() });
	});
});

router.post('/edit_product/:id', function(req, res, next){
	var sale = false, feature = false, anew = false, bests = false;
	if(req.body.attribSale == "1")
		sale = true;
	if(req.body.attribFeature == "1")
		feature = true;
	if(req.body.attribNew == "1")
		anew = true;
	if(req.body.attribBestseller == "1")
		bests = true;
	//create new product
	var productUpdate = { name: req.body.name, 
		category: req.body.category, 
		manufacturer: req.body.manufacturer, 
		availability: req.body.availability, 
		featured : feature,
		recentlyrelease : anew,
		bestseller: bests,
		onsale: sale,
		price: req.body.price,
		pricesale: req.body.pricesale, 
		genre: req.body.genre,
		website: req.body.website };

	Product.findOneAndUpdate({_id: req.params.id}, 
		productUpdate,
	 	{new: true}, function(err, product){
		res.render('edit_product', {product: product, _csrfToken: req.csrfToken() });
	});
});

//------Require for Upload file----------
var path = require('path');
var multer = require('multer');


var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, 'public/img/product/');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

var upload = multer({ storage: storage });

router.get('/add_product', function(req, res, next){
	res.render('add_product', {formError: req.flash('formError'), _csrfToken: req.csrfToken() });
});



router.post('/add_product/POST', upload.any(), function(req, res, next){
	alert("All files have uploaded ");
	console.log(req.files);
});



router.post('/add_product', fieldValidation, function(req, res, next){
	var sale = false, feature = false, anew = false, bests = false;
	if(req.body.attribSale == "1")
		sale = true;
	if(req.body.attribFeature == "1")
		feature = true;
	if(req.body.attribNew == "1")
		anew = true;
	if(req.body.attribBestseller == "1")
		bests = true;
	//create new product
	var newProduct = new Product({ name: req.body.name, 
		category: req.body.category, 
		manufacturer: req.body.manufacturer, 
		availability: req.body.availability, 
		featured : feature,
		recentlyrelease : anew,
		bestseller: bests,
		onsale: sale,
		price: req.body.price,
		pricesale: req.body.pricesale, 
		genre: req.body.genre,
		website: req.body.website });

	if(req.flash != undefined){
		res.render('add_product', { formError: req.flash('formError'), product: newProduct, _csrfToken: req.csrfToken() });
	}
	//save to db
	newProduct.save(function(err){
		if (err) {
			return err;
		}
	  	else {
		  	Product.find(function(err, products, count){
				res.render('product_list', { products: products, _csrfToken: req.csrfToken() });
			});
		}
	});
});

//-------------------Add part in coupons---------------------
router.get('/coupon_list', function(req, res, next){
	Coupon.find(function(err, coupons, count){
		res.render('coupon_list', { coupons: coupons, _csrfToken: req.csrfToken() });
	});
});

router.get('/add_coupon', function(req, res, next){
	res.render('add_coupon', { _csrfToken: req.csrfToken() });
});

router.post('/add_coupon', function(req, res, next){
	//create new coupon
	var newCoupon = new Coupon({ 
		name: req.body.name, 
		code: req.body.code, 
		startDate: req.body.startDate, 
		expiredDate: req.body.expiredDate });

	//save to db
	newCoupon.save(function(err){
		if (err) {
			return err;
		}
	  	else {
		  	Coupon.find(function(err, coupons, count){
				res.render('coupon_list', { coupons: coupons, _csrfToken: req.csrfToken() });
			});
		}
	});
});

router.get('/edit_coupon/:id', function(req, res, next){
	Coupon.findById(req.params.id, function(err, coupon){
		res.render('edit_coupon', { coupon: coupon, _csrfToken: req.csrfToken() });
	});
});

router.post('/edit_coupon', function(req, res, next){

	Coupon.findOneAndUpdate({_id: req.body.id},{$set:{name: req.body.name, code: req.body.code,
		startDate: req.body.startDate, expiredDate:req.body.expiredDate}}, {new : true} ,function(err, coupon){
		res.render('edit_coupon', { coupon: coupon, _csrfToken: req.csrfToken() });
	});
});
var mongodb = require('mongodb');

// db.collection('posts', function(err, collection) {
//    collection.deleteOne({_id: new mongodb.ObjectID('4d512b45cc9374271b00000f')});
// });
router.post('/delete_coupon', function(req, res, next){
	//Coupon.find({ _id: req.body.id}).remove().exec();
	Coupon.find({ _id: req.body.id}).remove().exec();
	Coupon.find(function(err, users, count){
		res.redirect('/admin/coupon_list');
	});
});

//-------------------Account part--------------------------

router.get('/account', function(req, res, next){
	User.find(function(err, users, count){
		res.render('manage_account', { users: users, _csrfToken: req.csrfToken() });
	});
});

router.post('/delete_account', function(req, res, next){
	User.find({ _id: req.body.id}).remove().exec();
	User.find(function(err, users, count){
		res.redirect('/admin/account');
	});
});

router.get('/lock_account/:id', function(req, res, next){
	User.findOneAndUpdate({_id: req.params.id},{$set:{'local.locked':true}},function(err, user){
		res.redirect('back');
	});
});

router.get('/unlock_account/:id', function(req, res, next){
	User.findOneAndUpdate({_id: req.params.id}, {$set:{'local.locked':false}},function(err, user){
		res.redirect('back');
	});
});

function fieldValidation(req, res, next){
	req.checkBody('name', 'Tên sản phẩm không được bỏ trống').notEmpty();
	req.checkBody('availability', 'Số lượng sản phẩm không hợp lệ').isInt();

	var errors = req.validationErrors();
	if(errors){
        // req.session.errors = error;
        errors.forEach(function(currentValue){
            req.flash('formError', currentValue.msg);
        });
        next();
    } 
}

module.exports = router;