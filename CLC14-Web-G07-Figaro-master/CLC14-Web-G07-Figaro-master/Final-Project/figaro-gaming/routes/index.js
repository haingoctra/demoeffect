var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var csrfProtection = csrf();
var Product = require('../models/product');
var News = require('../models/news');
var Cart = require('../models/cart');
var Compare = require('../models/compare');
router.use(csrfProtection);

router.get('/', queryFeaturedProduct, queryOnSaleProduct,queryBestSellerProduct,queryRecentlyReleaseProduct, 
	function(req, res, next){
	// Product.find(function(err, products, count){
	// 	res.render('index', { title: 'Figaro Gaming', _csrfToken: req.csrfToken(), user: req.user, products:products, 
	// 	featuredproducts : req.featured, onsaleproducts : req.onsale, bestsellerproducts:req.bestseller, recentlyreleaseproducts:req.recentlyrelease  });

	// });
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}

	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}


	res.render('index', { title: 'Figaro Gaming', _csrfToken: req.csrfToken(), 
		user: req.user, 
		featuredproducts : req.featured, 
		onsaleproducts : req.onsale, 
		bestsellerproducts:req.bestseller, 
		recentlyreleaseproducts:req.recentlyrelease, 
		cart : _cart,compare: _compare });

});

router.get('/compare', queryFeaturedProduct, queryOnSaleProduct,queryBestSellerProduct,queryRecentlyReleaseProduct, 
	function(req, res, next){
	// Product.find(function(err, products, count){
	// 	res.render('index', { title: 'Figaro Gaming', _csrfToken: req.csrfToken(), user: req.user, products:products, 
	// 	featuredproducts : req.featured, onsaleproducts : req.onsale, bestsellerproducts:req.bestseller, recentlyreleaseproducts:req.recentlyrelease  });

	// });
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}

	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}


	res.render('compare', { title: 'So sánh sản phẩm', _csrfToken: req.csrfToken(), 
		user: req.user, 
		featuredproducts : req.featured, 
		onsaleproducts : req.onsale, 
		bestsellerproducts:req.bestseller, 
		recentlyreleaseproducts:req.recentlyrelease, 
		cart : _cart,compare: _compare });

});




router.get('/featured',queryFeaturedProduct, 
	queryActionProducts,
	queryRPGProducts,
	querySportProducts,
	queryAdventureProducts,
	queryFPSProducts,
	queryStrategyProducts,
	queryGenreProduct,
	function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}

	var sortby = 0,limit = 9;

	if(req.query.sortby){
		sortby = req.query.sortby;
	}


	if(req.query.limit){
		limit = req.query.limit;
	}


	if(sortby == 1){
			Product.find({ featured:'true'})
			.sort({price:1})
			.limit(parseInt(limit))
			.skip(parseInt(limit)*page)
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Featured', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
	else{
		if(sortby == 2){
			Product.find({ featured:'true'})
			.sort({price:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Featured', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
		if(sortby == 3){
			Product.find({ featured:'true'})
			.sort({$name:1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Featured', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}

		if(sortby == 4){
			Product.find({ featured:'true'})
			.sort({$name:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Featured', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
		else{
			Product.find({ featured:'true'})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Featured', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
	}
});

router.get('/topseller',queryBestSellerProduct, 
	function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}

	var sortby = 0,limit = 9;

	if(req.query.sortby){
		sortby = req.query.sortby;
	}


	if(req.query.limit){
		limit = req.query.limit;
	}


	if(sortby == 1){
			Product.find({ bestseller:'true'})
			.sort({price:1})
			.limit(parseInt(limit))
			.skip(parseInt(limit)*page)
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Best seller', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
	else{
		if(sortby == 2){
			Product.find({ bestseller:'true'})
			.sort({price:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Best seller', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
		if(sortby == 3){
			Product.find({ bestseller:'true'})
			.sort({$name:1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Best seller', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}

		if(sortby == 4){
			Product.find({ bestseller:'true'})
			.sort({$name:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Best seller', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
		else{
			Product.find({ bestseller:'true'})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Best seller', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
	}
});

router.get('/recentlyrelease',queryRecentlyReleaseProduct, 
	function(req, res, next) {
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}

	var sortby = 0,limit = 9;

	if(req.query.sortby){
		sortby = req.query.sortby;
	}


	if(req.query.limit){
		limit = req.query.limit;
	}


	if(sortby == 1){
			Product.find({ recentlyrelease:'true'})
			.sort({price:1})
			.limit(parseInt(limit))
			.skip(parseInt(limit)*page)
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Recently Release', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
	else{
		if(sortby == 2){
			Product.find({ recentlyrelease:'true'})
			.sort({price:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Recently Release', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
		if(sortby == 3){
			Product.find({ recentlyrelease:'true'})
			.sort({$name:1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Recently Release', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}

		if(sortby == 4){
			Product.find({ recentlyrelease:'true'})
			.sort({$name:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Recently Release', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
		else{
			Product.find({ recentlyrelease:'true'})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'Recently Release', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
	}
});


router.get('/onsale',queryOnSaleProduct, 
	function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}

	var sortby = 0,limit = 9;

	if(req.query.sortby){
		sortby = req.query.sortby;
	}


	if(req.query.limit){
		limit = req.query.limit;
	}


	if(sortby == 1){
			Product.find({ onsale:'true'})
			.sort({price:1})
			.limit(parseInt(limit))
			.skip(parseInt(limit)*page)
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'On Sale', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
	else{
		if(sortby == 2){
			Product.find({ onsale:'true'})
			.sort({price:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'On Sale', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
		if(sortby == 3){
			Product.find({ onsale:'true'})
			.sort({$name:1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'On Sale', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}

		if(sortby == 4){
			Product.find({ onsale:'true'})
			.sort({$name:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'On Sale', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
		else{
			Product.find({ onsale:'true'})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('topseller', { title: 'On Sale', _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category});
			});
		}
	}
});

function queryGenreProduct(req, res, next){

	var genrefilter= req.body.genrefilter;

	Product.find({ genre:'genrefilter'}, function(err, filterproducts, count){
		req.filter = filterproducts;
		next();
	});
}


function queryFeaturedProduct(req, res, next){
	Product.find({ featured:'true'}, function(err, featuredproducts, count){
		req.featured = featuredproducts;
		next();
	});
}


function queryBestSellerProduct(req, res, next){
	Product.find({ bestseller:'true'}, function(err, bestsellerproducts, count){
		req.bestseller = bestsellerproducts;
		next();
	})
}


function queryOnSaleProduct(req, res, next){
	Product.find({ onsale:'true'}, function(err, onsaleproducts, count){
		req.onsale = onsaleproducts;
		next();
	})
}


function queryRecentlyReleaseProduct(req, res, next){
	Product.find({ recentlyrelease:'true'}, function(err, recentlyreleaseproducts, count){
		req.recentlyrelease = recentlyreleaseproducts;
		next();
	})
}

function queryActionProducts(req, res, next){
	Product.find({ genre:'action'}, function(err,actionproducts , count){
		req.action = actionproducts;
		next();
	});
}

function queryRPGProducts(req, res, next){
	Product.find({ genre:'rpg'}, function(err, rpgproducts, count){
		req.rpg = rpgproducts;
		next();
	});
}


function querySportProducts(req, res, next){
	Product.find({ genre:'sport'}, function(err, sportproducts, count){
		req.sport = sportproducts;
		next();
	});
}

function queryAdventureProducts(req, res, next){
	Product.find({ genre:'adventure'}, function(err, adventureproducts, count){
		req.adventure = adventureproducts;
		next();
	});
}


function queryFPSProducts(req, res, next){
	Product.find({ genre:'fps'}, function(err, fpsproducts, count){
		req.fps = fpsproducts;
		next();
	});
}


function queryStrategyProducts(req, res, next){
	Product.find({ genre:'strategy'}, function(err, strategyproducts, count){
		req.strategy = strategyproducts;
		next();
	});
}


router.get('/news', function(req, res, next) {
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	News.find(function(err, news, count){
  		res.render('news', { title: 'Tin tức', _csrfToken: req.csrfToken(), user: req.user, news:news, cart : _cart ,compare: _compare });
	});
});


router.get('/actiongames', queryActionProducts,function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	res.render('genre', { title: 'Action games', _csrfToken: req.csrfToken(), user: req.user, 
		 products:req.action, cart : _cart,compare: _compare });

});


router.get('/rpggames', queryRPGProducts,function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	res.render('genre', { title: 'RPG games', _csrfToken: req.csrfToken(), user: req.user, 
		 products:req.rpg, cart : _cart,compare: _compare });

});


router.get('/sportgames',querySportProducts, function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	res.render('genre', { title: 'Sport games', _csrfToken: req.csrfToken(), user: req.user, 
		 products:req.sport, cart : _cart,compare: _compare });
});


router.get('/adventuregames',queryAdventureProducts, function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	res.render('genre', { title: 'Adventure games', _csrfToken: req.csrfToken(), user: req.user, 
		 products:req.adventure, cart : _cart,compare: _compare });
});


router.get('/fpsgames',queryFPSProducts, function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	res.render('genre', { title: 'FPS games', _csrfToken: req.csrfToken(), user: req.user, 
		 products:req.fps, cart : _cart,compare: _compare });
});

router.get('/strategygames',queryStrategyProducts, function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	res.render('genre', { title: 'Strategy games', _csrfToken: req.csrfToken(), user: req.user, 
		 products:req.strategy, cart : _cart,compare: _compare });
});

router.get('/terms_conditions', function(req, res, next) {
	//var session = req.session;
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
  	res.render('terms_conditions', { title: 'Điều khoản dịch vụ', _csrfToken: req.csrfToken(), user: req.user, cart: _cart,compare: _compare });
});

router.get('/privacy_policy', function(req, res, next) {
	//var session = req.session;
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
  	res.render('privacy_policy', { title: 'Chính sách bảo mật', _csrfToken: req.csrfToken(), user: req.user, cart : _cart,compare: _compare  });
});


router.get('/copyrightgame', function(req, res, next) {
	//var session = req.session;
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
  	res.render('copyrightgame', { title: 'Game bản quyền', _csrfToken: req.csrfToken(), user: req.user, cart : _cart,compare: _compare  });
});


router.get('/aboutus', function(req, res, next) {
	//var session = req.session;
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
  	res.render('aboutus', { title: 'About us', _csrfToken: req.csrfToken(), user: req.user, cart : _cart,compare: _compare  });
});


router.get('/contact', function(req, res, next) {
	//var session = req.session;
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
  	res.render('contact', { title: 'About us', _csrfToken: req.csrfToken(), user: req.user, cart : _cart,compare: _compare  });
});


router.get('/guarantee_policy', function(req, res, next) {
	//var session = req.session;
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
  	res.render('guarantee_policy', { title: 'Bảo hành', _csrfToken: req.csrfToken(), user: req.user, cart : _cart,compare: _compare  });
});


router.get('/logout', function(req, res, next) {
	req.session.destroy();
	res.redirect('/');
});

module.exports = router;
