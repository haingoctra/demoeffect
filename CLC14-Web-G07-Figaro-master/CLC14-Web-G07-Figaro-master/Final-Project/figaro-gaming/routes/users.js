var express = require('express');
var router = express.Router();
var User = require('../models/user');
var csrf = require('csurf');
var csrfProtection = csrf();
router.use(csrfProtection);

/* GET users listing. */
router.get('/', function(req, res, next) {
	var _cart = [];
    if (req.session.cart) {
        var myCart = new Cart(req.session.cart);
        _cart = myCart.toArray();
    }
    var _compare = [];
    if (req.session.compare) {
        var myCompare = new Compare(req.session.compare);
        _compare = myCompare.toArray();
    }

    User.findOne({'local.username': req.user.local.username}, function(err, user){
		res.render('account_info', { title: 'Thông tin tài khoản', user: user, 
			_csrfToken: req.csrfToken(), cart : _cart,compare: _compare });
	});

});

router.get('/change_password', function(req, res, next) {
	var _cart = [];
    if (req.session.cart) {
        var myCart = new Cart(req.session.cart);
        _cart = myCart.toArray();
    }
    var _compare = [];
    if (req.session.compare) {
        var myCompare = new Compare(req.session.compare);
        _compare = myCompare.toArray();
    }
	res.render('change_password', { title: 'Cập nhật mật khẩu', user: req.user, 
        	flashMessage: req.flash('flashMessage'),
			_csrfToken: req.csrfToken(), cart : _cart,compare: _compare });
	
});

router.post('/update_password', function(req, res, next){
	// req.flash('flashMessage') = [];
	var _cart = [];
    if (req.session.cart) {
        var myCart = new Cart(req.session.cart);
        _cart = myCart.toArray();
    }
    var _compare = [];
    if (req.session.compare) {
        var myCompare = new Compare(req.session.compare);
        _compare = myCompare.toArray();
    }
	//validate fields
	req.checkBody('oldpass', 'Mật khẩu cũ không được bỏ trống').notEmpty();
	req.checkBody('newpass', 'Mật khẩu mới không được bỏ trống').notEmpty();
	req.checkBody('newpassconf', 'Mật khẩu mới không được bỏ trống').notEmpty();
	req.checkBody('newpass', 'Mật khẩu tối thiếu 6 ký tự').isLength({min:6});
	req.checkBody('newpass', 'Mật khẩu không giống nhau').equals(req.body.newpassconf);

	User.findOne({ 'local.username' :  req.user.local.username }, function(err, user) {
		if (!user.validPassword(req.body.oldpass)){
            req.flash('flashMessage', 'Sai mật khẩu'); 
            res.render('change_password', { title: 'Cập nhật mật khẩu', user: req.user, 
        	flashMessage: req.flash('flashMessage'),
			_csrfToken: req.csrfToken(), cart : _cart,compare: _compare });
            // res.redirect('/users/change_password');
		}
    });

	var errors = req.validationErrors();
    if(errors){
        // req.session.errors = error;
        errors.forEach(function(currentValue){
            req.flash('flashMessage', currentValue.msg);
        });
        res.render('change_password', { title: 'Cập nhật mật khẩu', user: req.user, 
        	flashMessage: req.flash('flashMessage'),
			_csrfToken: req.csrfToken(), cart : _cart,compare: _compare });
			// res.redirect('/users/change_password');
			// res.end();
    }
    else{
    	var newUser = new User();                                                                                                               
        newUser.local.password = newUser.generateHash(req.body.newpass);

    	User.findOneAndUpdate({'local.username': req.user.local.username},
    	{$set:{'local.password':newUser.local.password}},function(err, user){
			res.redirect('/users');
	});
    }

});

module.exports = router;
