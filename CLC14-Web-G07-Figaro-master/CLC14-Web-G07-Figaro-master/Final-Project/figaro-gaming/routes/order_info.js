var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var csrfProtection = csrf();
router.use(csrfProtection);
var Cart = require('../models/cart');
var Compare = require('../models/compare');

router.get('/', function(req, res, next){
	// var pid = req.params.id;
	// var cart = new Cart(req.session.cart ? req.session.cart : {items:{}});

	var _cart = [];
	var _totalPrice = 0;
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
		_totalPrice = myCart.totalPrice;
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	if(!req.user)
		res.redirect('/signup');
	else{
		res.render('order_info', { title: 'Thông tin giỏ hàng', _csrfToken: req.csrfToken(),
		    user: req.user,  cart: _cart, totalPrice: _totalPrice,compare: _compare });
	}
});

module.exports = router;
