var express = require('express');
var router = express.Router();
var passport = require('passport');
var User = require('../models/user');

router.post('/', passport.authenticate('local-login', {
  failureRedirect: '/signup',
  failureFlash: true
}),
	function(req, res){
		if(req.user.local.type == 'admin')
			res.redirect('/admin');
		else res.redirect('/');
	}
);


module.exports = router;