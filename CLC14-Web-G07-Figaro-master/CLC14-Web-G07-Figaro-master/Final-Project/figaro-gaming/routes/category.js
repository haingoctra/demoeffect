 var express = require('express');
var router = express.Router();
var Product = require('../models/product');
var Cart = require('../models/cart');
var csrf = require('csurf');
var csrfProtection = csrf();
router.use(csrfProtection);


// router.get('/:category', function(req, res, next) {

// 	var _cart = [];
// 	if (req.session.cart) {
// 		var myCart = new Cart(req.session.cart);
// 		_cart = myCart.toArray();
// 	}
// 	var cat = (req.params.category).toLowerCase();

// 	Product.find({ category:cat}, function (err, steamproducts, count) {
// 	  res.render('category', { title: req.params.category, _csrfToken: req.csrfToken(), user: req.user, 
// 	  	products: steamproducts, cart : _cart });
// 	});
// });



router.get('/:category', function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}

	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Cart(req.session.compare);
		_compare = myCompare.toArray();
	}
	var cat = (req.params.category).toLowerCase();

	var sortby = 0,limit = 9;
	//var genrefilter = "all";
	//var genrefilter = ["action","rpg","sport","adventure","fps","strategy"];
	var genrefilter = req.query.genrefilter;

	if(req.query.sortby){
		sortby = req.query.sortby;
	}

	if(req.query.genrefilter){
		genrefilter = req.query.genrefilter;
	}

	if(req.query.limit){
		limit = req.query.limit;
	}

	var from = 0;
	var to = 1000000;

	


	if(req.query.from){
		from = req.query.from;
	}
	if(req.query.to){
		to = req.query.to;
	}
	console.log(from + to);


	if(sortby == 1){
			Product.find({category:cat})
			.sort({price:1})
			.limit(parseInt(limit))
			.skip(parseInt(limit)*page)
			.exec(function(err, steamproducts, count){
				
				res.render('category', { title: req.params.category, _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category,from:from,to:to});
			});
		}
	else{
		if(sortby == 2){
			Product.find({category:cat})
			.sort({price:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('category', { title: req.params.category, _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category,from:from,to:to});
			});
		}
		if(sortby == 3){
			Product.find({category:cat})
			.sort({$name:1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('category', { title: req.params.category, _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby,limit:limit, cart : _cart, compare: _compare, category: req.params.category,from:from,to:to});
			});
		}

		if(sortby == 4){
			Product.find({category:cat})
			.sort({$name:-1})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('category', { title: req.params.category, _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby, genrefilter: genrefilter,limit:limit, cart : _cart, compare: _compare, category: req.params.category,from:from,to:to});
			});
		}
		else{
			Product.find({category:cat})
			.limit(parseInt(limit))
			.exec(function(err, steamproducts, count){
				
				res.render('category', { title: req.params.category, _csrfToken: req.csrfToken(), user: req.user,
					products:steamproducts,sortby:sortby, genrefilter: genrefilter,limit:limit, cart : _cart, compare: _compare, category: req.params.category,from:from,to:to});
			});
		}
	}
});



// router.get('/', function(req, res, next) {
// 	//var status = ["new","hot","sale"];
// 	//var category = req.query.category;
// 	var sortby = 0,limit = 9;
	
// 	if(req.query.sortby){
// 		sortby = req.query.sortby;
// 	}
	// if(req.query.limit){
	// 	limit = req.query.limit;
	// }
	// if (req.query.state=="new" || req.query.state=="hot" ||req.query.state=="sale"){
	// 	var status = req.query.state;
	// }
	// if(req.query.from){
	// 	from = req.query.from;
	// }
	// if(req.query.to){
	// 	to = req.query.to;
	// }
	// console.log(from + to);

	// if(sortby == 1){
	// 	Product.find({category:category, status:status, price:{$gte: parseFloat(from), $lte: parseFloat(to)}})
	// 		.sort({price:1})
	// 		.limit(parseInt(limit))
	// 		.exec(function(err,products){
	// 			var title = category.charAt(0).toUpperCase() + category.slice(1);
	// 			res.render('productgird', { title: title, products:products,query:category, sortby:sortby, limit:limit, state:status,from: from, to:to });
	// 		});
	// }

// 	var from = 0;
// 	var to = 1000000;
	
// 	if(sortby == 1){
// 		Product.find({price:{$gte: parseFloat(from), $lte: parseFloat(to)}})
// 			.sort({price:1})
// 			.limit(parseInt(limit))
// 			.exec(function(err,products){
				
// 				res.render('category', { products:products,sortby:sortby});
// 			});
// 	}
// 	else{
// 		if(sortby == 2){
// 			Product.find({price:{$gte: parseFloat(from), $lte: parseFloat(to)}})
// 			.sort({price:-1})
// 			.limit(parseInt(limit))
// 			.exec(function(err,products){
				
// 				res.render('category', { products:products,sortby:sortby});
// 			});
// 		}
// 		else{
// 			Product.find({price:{$gte: parseFloat(from), $lte: parseFloat(to)}})
// 			.limit(parseInt(limit))
// 			.exec(function(err,products){
				
// 				res.render('category', { products:products,sortby:sortby});
// 			});
// 		}
// 	}	
// });




module.exports = router;