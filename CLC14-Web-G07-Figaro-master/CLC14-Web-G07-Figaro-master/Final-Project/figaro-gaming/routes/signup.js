var express = require('express');
var router = express.Router();
var passport = require('passport');
var https = require('https');
var csrf = require('csurf');
var csrfProtection = csrf();
router.use(csrfProtection);

var User = require('../models/user');
var Cart = require('../models/cart');
var Compare = require('../models/compare');

router.get('/', function(req, res, next) {
    var _cart = [];
    if (req.session.cart) {
        var myCart = new Cart(req.session.cart);
        _cart = myCart.toArray();
    }
    var _compare = [];
    if (req.session.compare) {
        var myCompare = new Compare(req.session.compare);
        _compare = myCompare.toArray();
    }
  	res.render('signup', { title: 'Đăng ký tài khoản', _csrfToken: req.csrfToken(), 
  		signupMessage: req.flash('signupMessage'), loginMessage: req.flash('loginMessage'), cart : _cart,compare: _compare });

});

router.post('/', function (req, res, next) {
	// req.check('loginuser', 'Username trống').notEmpty();
	// req.check('loginpass', 'Password trống').notEmpty();
	// var errors = req.validationErrors();
 //    if(!errors){ 
 //        passport.authenticate('local-login', {
	//   	successRedirect: '/',
	// 	failureRedirect: '/signup',
	// 	failureFlash: true});
 //    }

    verifyRecaptcha(req.body['g-recaptcha-response'], function (success) {
        if (success) {
            return next(); 
        } else {
            res.end('Captcha failed, sorry.');
            // TODO: take them back to the previous page
            // and for the love of everyone, restore their inputs
            return false;
        }
    });
}, 
	passport.authenticate('local-signup', {
  	successRedirect: '/',
	failureRedirect: '/signup',
	failureFlash: true
})

);



function verifyRecaptcha(key, callback) {
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=6LefSB4UAAAAAEi2Qe5FXT5PvD9XWF12Do-2g_Sr&response=" 
    	+ key, function(res) {
        var data = "";
        res.on('data', function (chunk) {
                        data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                console.log(parsedData);
                callback(parsedData.success);
            } catch (e) {
                callback(false);
            }
        });
    });
}
module.exports = router;
