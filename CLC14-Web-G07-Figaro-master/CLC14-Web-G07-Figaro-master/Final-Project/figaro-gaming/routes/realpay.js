var express = require('express');
var paypal = require('paypal-rest-sdk');
var router = express.Router();
var Cart = require('../models/cart');
var Compare = require('../models/compare');
var csrf = require('csurf');
var csrfProtection = csrf();
router.use(csrfProtection);
/* GET home page. */
router.get('/', function(req, res, next) {

    var _cart = [];
    var _totalPrice = 0;
    if (req.session.cart) {
        var myCart = new Cart(req.session.cart);
        _cart = myCart.toArray();
        _totalPrice = myCart.totalPrice;
    }
    var _compare = [];
    if (req.session.compare) {
        var myCompare = new Compare(req.session.compare);
        _compare = myCompare.toArray();
    }


    var payment = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "/execute",
            "cancel_url": "/cancel"
        },
        "transactions": [{
            "amount": {
                "total": "50.00",
                "currency": "USD"
            },
            "description": "Payment for order number: 78123"
        }]
    };

    paypal.payment.create(payment, function(error, payment) {
        if (error) {
            console.log(error);
        } else {
            if (payment.payer.payment_method === 'paypal') {
                req.session.paymentId = payment.id;

                var redirectUrl;
                for (var i = 0; i < payment.links.length; i++) {
                    var link = payment.links[i];
                    if (link.method === 'REDIRECT') {
                        redirectUrl = link.href;
                    }
                }
                res.redirect(redirectUrl);
            }
        }
    });
});

module.exports = router;