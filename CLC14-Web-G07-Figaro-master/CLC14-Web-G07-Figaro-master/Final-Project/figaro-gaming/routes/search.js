var express = require('express');
var router = express.Router();
var Product = require('../models/product');
var Cart = require('../models/cart');
var Compare = require('../models/compare');
/* GET home page. */



router.post('/', function(req, res, next) {
	var searchPhrase = req.body.keyword;
	console.log(searchPhrase);
	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	Product.find({ 'name':{ "$regex": searchPhrase, "$options": "i" } }, function(err, products, count){
		res.render('searchresult', {title: 'Kết quả tìm kiếm', products: products, 
			_csrfToken: req.csrfToken(), user: req.user, cart : _cart , searchPhrase: searchPhrase,compare: _compare });
	});
});
module.exports = router;