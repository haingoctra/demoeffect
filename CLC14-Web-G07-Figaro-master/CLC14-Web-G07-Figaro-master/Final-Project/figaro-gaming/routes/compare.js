var express = require('express');
var router = express.Router();
var Product = require('../models/product');

var Compare = require('../models/compare');

router.get('/:id', function(req, res, next){
	var pid = req.params.id;
	var compare = new Compare(req.session.compare ? req.session.compare : {items:{}});

	Product.findById(pid, function(error, product){
		compare.add(product, pid, 1);
		req.session.compare = compare;
		res.redirect('back');

		//res.redirect('/');
	});
});

router.post('/', function(req, res, next){
	var pid = req.body.id;
	var quantity = parseInt(req.body.quantity);
	var compare = new Compare(req.session.compare ? req.session.compare : {items:{}});

	Product.findById(pid, function(error, product){
		compare.add(product, pid, quantity);
		req.session.compare = compare;
		res.redirect('back');

		//res.redirect('/');
	});  
});

module.exports = router;
