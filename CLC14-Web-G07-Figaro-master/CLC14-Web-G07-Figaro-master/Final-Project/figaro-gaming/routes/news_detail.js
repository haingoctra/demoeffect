var express = require('express');
var router = express.Router();
var News = require('../models/news');
var Cart = require('../models/cart');
var Compare = require('../models/compare');
var csrf = require('csurf');
var csrfProtection = csrf();
router.use(csrfProtection);

router.get('/:id', function(req, res, next) {

	var _cart = [];
	if (req.session.cart) {
		var myCart = new Cart(req.session.cart);
		_cart = myCart.toArray();
	}
	var _compare = [];
	if (req.session.compare) {
		var myCompare = new Compare(req.session.compare);
		_compare = myCompare.toArray();
	}
	News.findById(req.params.id, function (err, news) {
	  res.render('news_detail', { news: news,  _csrfToken: req.csrfToken(), user: req.user, cart: _cart,compare: _compare });
	});
});


module.exports = router;