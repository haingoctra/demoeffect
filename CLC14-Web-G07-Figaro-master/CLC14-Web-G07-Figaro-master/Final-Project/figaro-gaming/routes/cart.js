var express = require('express');
var router = express.Router();
var Product = require('../models/product');

var Cart = require('../models/cart');

router.get('/:id', function(req, res, next){
	var pid = req.params.id;
	var cart = new Cart(req.session.cart ? req.session.cart : {items:{}});

	Product.findById(pid, function(error, product){
		cart.add(product, pid, 1);
		req.session.cart = cart;
		res.redirect('back');

		//res.redirect('/');
	});
});
	
router.post('/', function(req, res, next){
	var pid = req.body.id;
	var quantity = parseInt(req.body.quantity);
	var cart = new Cart(req.session.cart ? req.session.cart : {items:{}});

	Product.findById(pid, function(error, product){
		cart.add(product, pid, quantity);
		req.session.cart = cart;
		res.redirect('back');

		//res.redirect('/');
	});
});

router.get('/remove/:id', function(req, res, next){
	var pid = req.params.id;
	var cart = new Cart(req.session.cart ? req.session.cart : {items:{}});

		cart.remove(pid);
		req.session.cart = cart;
		res.redirect('back');
});

module.exports = router;
