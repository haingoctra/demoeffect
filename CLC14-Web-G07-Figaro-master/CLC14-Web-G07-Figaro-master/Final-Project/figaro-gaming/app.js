var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
var expressValidator = require('express-validator');
// var MongoStore = require('connect-mongo')(session);

var index = require('./routes/index');
var users = require('./routes/users');
var login = require('./routes/login');
var news_detail = require('./routes/news_detail');
var product_detail = require('./routes/product_detail');
var profile = require('./routes/profile');
var signup = require('./routes/signup');
var payment_method = require('./routes/payment_method');
var realpay = require('./routes/realpay');

var search = require('./routes/search');

var cart = require('./routes/cart');
var compare = require('./routes/compare');
var order_info = require('./routes/order_info');
var category = require('./routes/category');
var admin = require('./routes/admin');
var Cart = require('./models/cart');
var Compare = require('./models/compare');

var app = express();

var dbURI = 'localhost:27017/figaro-gaming';
if (process.env.NODE_ENV === 'production') {
    dbURI = process.env.MONGOLAB_URI;
}
mongoose.connect(dbURI);

// mongoose.connect('localhost:27017/figaro-gaming');
require('./config/passport')(passport);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// required for passport
app.use(session({
  secret: 'MySuperSecret',
  resave: false,
  saveUninitialized: true,
  // store: new MongoStore({mongooseConnnection: mongoose.connection}),
  cookie: { path: '/', httpOnly: true, maxAge: 30 * 30000 },
  rolling: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash());

app.use(expressValidator());

// Truyền session vào mọi response
app.use(function(req, res, next) {
  res.locals.session = req.session;
  //res.locals.user = req.user; //!-----
  next();
});

app.use('/', index);
app.use('/users', users);
app.use('/login', login);
app.use('/profile', profile);
app.use('/logout', index);
app.use('/signup', signup);
app.use('/news_detail', news_detail);
app.use('/product_detail', product_detail);
app.use('/contact', index);
app.use('/searchresult',search);
app.use('/add_to_cart', cart);
app.use('/add_to_compare', compare);
app.use('/order_info', order_info);
app.use('/category', category);
app.use('/admin', admin);
app.use('/payment_method', payment_method);
app.use('/realpay', realpay);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // var err = new Error('Not Found');
  // err.status = 404;
  // next(err);

  var _cart = [];
  if (req.session.cart) {
    var myCart = new Cart(req.session.cart);
    _cart = myCart.toArray();
  }
  var _compare = [];
  if (req.session.compare) {
    var myCompare = new Compare(req.session.compare);
    _compare = myCompare.toArray();
  }
    res.render('404', { title: 'Figaro gaming page not found', _csrfToken: req.csrfToken(), user: req.user, cart : _cart,compare: _compare  });

});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


//-----------TRYING FOR PAYMENT---------
var fs = require('fs');
var paypal = require('paypal-rest-sdk');

try {
    var configJSON = fs.readFileSync(__dirname + "/config.json");
    var config = JSON.parse(configJSON.toString());
} catch (e) {
    console.error('File config.json not found or is invalid: ' + e.message);
    process.exit(1);
}
paypal.configure(config.api);

app.use(session({
    secret: 'My Secret',
    resave: false,
    saveUninitialized: false
}));

app.use(function(req, res, next) {
    res.locals.session = req.session;
    next();
});


module.exports = app;
