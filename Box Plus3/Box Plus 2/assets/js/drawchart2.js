Chart.pluginService.register({
  afterDraw: function(chart) {
    if (chart.config.options.elements.center) {
      var helpers = Chart.helpers;
      var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
      var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;

      var ctx = chart.chart.ctx;
      ctx.save();
      var fontSize = helpers.getValueOrDefault(chart.config.options.elements.center.fontSize, Chart.defaults.global.defaultFontSize);
      var fontStyle = helpers.getValueOrDefault(chart.config.options.elements.center.fontStyle, Chart.defaults.global.defaultFontStyle);
      var fontFamily = helpers.getValueOrDefault(chart.config.options.elements.center.fontFamily, Chart.defaults.global.defaultFontFamily);
      var font = helpers.fontString(fontSize, fontStyle, fontFamily);
      ctx.font = font;
      ctx.fillStyle = helpers.getValueOrDefault(chart.config.options.elements.center.fontColor, Chart.defaults.global.defaultFontColor);
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.fillText(chart.config.options.elements.center.text, centerX, centerY);
      ctx.restore();
    }
  },
})


// each device

var data = [
  {
    id: '#p1',
    name: 'person 1',
    beers: [50,20,05,10,05,10]
  },
  {
    id: '#p2',
    name: 'person 1',
    beers: [20,50,05,10,05,10]
  },
  {
    id: '#p3',
    name: 'person 1',
    beers: [30,40,05,10,05,10]
  }
]


var color=["#5e9bd2","#ea7d3c","#8b572a","#fcbe2d","#4674c0","#72ac4d"];


var getOption = function(_percentSet, _pos,_color, countedpercent) {
  return {
    type: 'pie',
    data: {
      labels: [
        "Red",
        "Green",
        "Yellow"
      ],
      datasets: [{
        data: [countedpercent, _percentSet[_pos], 100-(_percentSet[_pos]+countedpercent)],
        backgroundColor: [
          "white",
          color[_color],
          "white"
        ],
        hoverBackgroundColor: [
          "white",
          color[_color],
          "white"
        ]
      }]
    },
    options: {
      elements: {
        center: {
          text: _percentSet[_pos].toString() + '%',
          fontColor: '#000',
          fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
          fontSize: 15,
          fontStyle: 'normal'
        }
      },

      segmentShowStroke: false,
      tooltips :{
          enabled: false
      },
      responsive: true,
      legend: {
        display: false
      }
    }
  };
}

var renderData = function(_data) {
  _data.forEach(function(person, index){

    var countedpercent = 0;

    person.beers.forEach(function(e,i) {
      var canvasElem = $(person.id + ' canvas').eq(i)[0].getContext('2d');
      var conf = getOption(person.beers, i, i, countedpercent);
      countedpercent = countedpercent + e;
        new Chart(canvasElem, conf);
    })

  })
}

renderData(data);