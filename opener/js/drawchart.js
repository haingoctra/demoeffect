




var dataheineken = {

  datasets: [
    {
      data: [20,100-20],
      backgroundColor: [
        "#5e9bd2",
        "white",
      ],
      borderColor: ['black','black'],
      borderWidth: 0.5
    }]
};

var heneikenchart = new Chart(document.getElementById('heneiken'), {
  type: 'pie',
  data: dataheineken,
  options: {
    segmentShowStroke: false,
    tooltips :{
        enabled: false
    },
    responsive: true,
    legend: {
      display: false
    }
  }
});

Chart.pluginService.register({
  beforeDraw: function(chart) {
    var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx;

    ctx.restore();
    var fontSize = (height / 114).toFixed(2);
    ctx.font = fontSize + "em sans-serif";
    ctx.textBaseline = "middle";

    var text = "20%",
        textX = Math.round((width - ctx.measureText(text).width) / 2),
        textY = height / 2;

    ctx.fillText(text, textX, textY);
    ctx.save();
  }
});



var datatiger = {

  datasets: [
    {
      data: [20,10,100-(20+10)],
      backgroundColor: [
        "white",
        "#ea7d3c",
        "white",
      ],
      borderColor: ['black','black','black'],
      borderWidth: 0.5
    }]
};

var tigerchar = new Chart(document.getElementById('tiger'), {
  type: 'pie',
  data: datatiger,
  options: {
    segmentShowStroke: false,
    tooltips :{
        enabled: false
    },
    responsive: true,
    legend: {
      display: false
    }
  }
});



var databud = {

  datasets: [
    {
      data: [20+10,30,100-(20+10+30)],
      backgroundColor: [
        "white",
        "#8b572a",
        "white",
      ],
      borderColor: ['black','black','black'],
      borderWidth: 0.5
    }]
};

var budchar = new Chart(document.getElementById('bud'), {
  type: 'pie',
  data: databud,
  options: {
    segmentShowStroke: false,
    tooltips :{
        enabled: false
    },
    responsive: true,
    legend: {
      display: false
    }
  }
});


var datasaigon = {

  datasets: [
    {
      data: [20+10+30,20,100-(20+10+30+20)],
      backgroundColor: [
        "white",
        "#fcbe2d",
        "white",
      ],
      borderColor: ['black','black','black'],
      borderWidth: 0.5
    }]
};

var saigonchar = new Chart(document.getElementById('saigon'), {
  type: 'pie',
  data: datasaigon,
  options: {
    segmentShowStroke: false,
    tooltips :{
        enabled: false
    },
    responsive: true,
    legend: {
      display: false
    }
  }
});


var datasaigons = {

  datasets: [
    {
      data: [20+10+30+20,15,100-(20+10+30+20+15)],
      backgroundColor: [
        "white",
        "#4674c0",
        "white",
      ],
      borderColor: ['black','black','black'],
      borderWidth: 0.5
    }]
};

var saigonschar = new Chart(document.getElementById('saigons'), {
  type: 'pie',
  data: datasaigons,
  options: {
    segmentShowStroke: false,
    tooltips :{
        enabled: false
    },
    responsive: true,
    legend: {
      display: false
    }
  }
});



var dataothers = {

  datasets: [
    {
      data: [20+10+30+20+15,5,100-(20+10+30+20+15+5)],
      backgroundColor: [
        "white",
        "#72ac4d",
        "white",
      ],
      borderColor: ['black','black','black'],
      borderWidth: 0.5
    }]
};

var others = new Chart(document.getElementById('others'), {
  type: 'pie',
  data: dataothers,
  options: {
    segmentShowStroke: false,
    tooltips :{
        enabled: false
    },
    responsive: true,
    legend: {
      display: false
    }
  }
});


